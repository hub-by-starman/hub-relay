#include <SPI.h>
#include <PubSubClient.h>
#include <ESP8266WiFi.h>



#define MQTT_SERVER "ENTER_HUB_IP"
#define MQTT_USER "ENTER_MQTT_USER"
#define MQTT_PASSWORD "ENTER_MQTT_PASSWORD"
#define relay_bus 5
const char* ssid = "ENTER_SSID";
const char* password = "ENTER_SSID_PASSWORD";


PubSubClient client;

const char* ssid = "VM7F2EEE6";
const char* password = "xuuk4awmhxRm";

char message_buff[100];
WiFiClient espClient;


void setup_wifi() {
  delay(10);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
  }

}

void setup()
{
  Serial.begin(115200);
  // initialize the digital pin as an output.
  pinMode(relay_bus, OUTPUT);     // Initialize the LED_BUILTIN pin as an output



  client = PubSubClient(MQTT_SERVER, 1883, callback, espClient);
}

void loop()
{
  if (!client.connected())
  {
      Serial.print("Connecting to wifi\n");

      setup_wifi();
      Serial.print("Connected to wifi\n");

      client.connect("arduino-mqtt", MQTT_USER, MQTT_PASSWORD);
      Serial.print("Connected to MQTT");

      client.publish("heat_control", "Heater Active");
      client.subscribe("heat_control");
  }


  // MQTT client loop processing
  client.loop();
}

// handles message arrived on subscribed topic(s)
void callback(char* topic, byte* payload, unsigned int length) {

  int i = 0;


  // create character buffer with ending null terminator (string)
  for(i=0; i<length; i++) {
    message_buff[i] = payload[i];
  }
  message_buff[i] = '\0';

  String msgString = String(message_buff);
  Serial.print("Recieved message: ");
  Serial.print(msgString);
  Serial.print("\n");


  if (msgString.equals("on")) {
    digitalWrite(relay_bus, LOW);
    client.publish("heat_control", "Device On");

  }

  else if (msgString.equals("off")){
    digitalWrite(relay_bus, HIGH);
    client.publish("heat_control", "Device Off");

  } 
}




